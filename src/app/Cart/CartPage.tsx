import React, { useEffect, useState } from "react";
import CartItem from './CartItem';
import {useDispatch,useSelector} from 'react-redux';
import { StoreDto } from "../Types";
const CartPage = () => {
   // const dispatch =useDispatch();

   const myData = useSelector((state:StoreDto )=> state.cart);
   debugger;


    return (<div>
       Shopping cart ...({myData.length}) item(s)
       {myData.map(function(item, index){
                    return  <CartItem itemDetails={item}></CartItem>;
        })}
    </div>);

}
export default CartPage;