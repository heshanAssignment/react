import { CardMedia, Grid, Paper } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Container, Row, Col } from 'reactstrap';
import Anchor from '../../images/anchor.jpg';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }),
);

const CartItem = (props:any) => {
    const classes = useStyles();

    const styles = 
    { media: {   width:"4%" } };
    return (<>
        

    
    <div className={classes.root}>
      <Grid container spacing={3}>
       
        <Grid item xs={6} sm={4}>
          <Paper className={classes.paper}><CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="40"
                    width="4%"
                    image={Anchor}
                    title="Contemplative Reptile"
                    style={styles.media}
                /></Paper>
        </Grid>
        <Grid item xs={6} sm={4}>
          <Paper className={classes.paper}>{(props && props.itemDetails)?props.itemDetails.quantity:null}</Paper>
        </Grid>
        <Grid item xs={6} sm={4}>
          <Paper className={classes.paper}>{(props && props.itemDetails)?props.itemDetails.price:null}</Paper>
        </Grid>
       
      </Grid>
    </div>
    </>);
}

export default CartItem;