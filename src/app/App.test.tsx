import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import ProductsPage from '../products/ProductsPage';
import { productsApi } from '../api/productsApi';
import ProductItem from '../products/ProductItem';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import CartReducer from './redux/Cart/CartReducer';
import { Provider } from 'react-redux';
import { StoreDto } from './Types';
import thunk from 'redux-thunk';
import rootReducer from './redux/RootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import TopBar from '../shared/TopBar';
test('there are two products in the products page', () => {
  var products;
  productsApi.getProducts().then(res => {
    products=res;
    var productPage=<ProductItem products={products}></ProductItem>
    render(productPage);

    const linkElement = screen.getAllByRole('heading',{name:/lizard/i});
    var entries=linkElement.length;
    screen.debug();
    expect(entries).toEqual(2);
  });
  });

  test('clicking on add to cart button updates cart', () => {
    
    var products;
    productsApi.getProducts().then(res => {
     let store = createTheStore();
      products=res;

      var page=<Provider store={store}>
           <TopBar></TopBar>
          <ProductItem products={products}></ProductItem>
      </Provider>
      
      render(page);
      const linkElement = screen.getAllByRole('heading',{name:/lizard/i});
      let text="";
      var entries=linkElement.length;
      if(entries>0){

        //   //button/span[text()='Add To Cart']
        var myElement = document.getElementById("addtocart1");
        myElement?.click();
        var myElement= document.getElementById("cartCount");
        if(myElement){
         text=myElement?.textContent?myElement?.textContent:"";
         debugger;
        }
       
      }
      screen.debug();
      expect(text).toEqual(1);
    });
  });

   function createTheStore(){
    
    const initialstate:StoreDto={
      cart:[]
    };
    
    const middleware = [thunk];
    
    const store = createStore(rootReducer,initialstate,composeWithDevTools(applyMiddleware(...middleware)))
    
    return store;
    }

   

 