
type CartItemDto ={

  productId:number,
  price:number,
  quantity:number
}
type StoreDto={

  cart:CartItemDto[]
}

export type{CartItemDto,StoreDto};

