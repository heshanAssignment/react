import {combineReducers} from 'redux';
import CartReducer from './Cart/CartReducer'

var rootReducer= combineReducers({cart:CartReducer});

export default rootReducer;