
import React from 'react';
import { Switch, Route, HashRouter as Router } from 'react-router-dom';
import TopBar from '../shared/TopBar';
import { GetRoutes } from './routes';

function App() {

  return (
    <>
      <Router>
        <TopBar />
        <Switch>
          {GetRoutes().map(route => (
            <Route
              key={route.path || 'nopath'}
              path={route.path}
              exact={route.exact}
              component={route.component}
            />
          ))}
        </Switch>
      </Router>
    </>
  );
}

export default App;
