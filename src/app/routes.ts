
import React from "react";
import ProductsPage from "../products/ProductsPage";
import CartPage from "./Cart/CartPage";

type OFFeltRoute = {
    path?: string;
    exact?: boolean;
    component: React.ComponentType<any>;
}

export const GetRoutes = (): OFFeltRoute[] => {
    return [

        {
            path: "/",
            exact: true,
            component: ProductsPage
        },{
            path:"/cart",
            exact:true,
            component: CartPage
        }

    ];
};



