import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useSelector } from 'react-redux';
import { StoreDto } from '../app/Types';
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }),
);

const TopBar = () => {
    const classes = useStyles();
    const myData = useSelector((state:StoreDto )=> state.cart);
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    {/* <Typography variant="h6" className={classes.title} component={Link} to="">
                        Super Shopping..
                    </Typography> */}
                    <Typography variant="h6" className={classes.title} >
                        Super Shopping..
                    </Typography>
                   
                    
                    {/* <Button color="inherit"  component={Link} to="/cart">Shopping cart ({myData.length} item(s))</Button> */}
                    <Button color="inherit" >Shopping cart (<span id="cartCount">{myData.length}</span> item(s))</Button>

                   
                    <Button color="inherit">Login</Button>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default TopBar;

