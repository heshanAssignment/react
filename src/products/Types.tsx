export type Product={
    id:number,
    name:string,
    price:number,
    count:number,
    imageId:number
}