import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Anchor from '../images/anchor.jpg';
import { useDispatch } from 'react-redux';
import { cartAction } from '../app/redux/Cart/Actions/CartAction';
import {CartItemDto} from '../app/Types';
import { Product } from './Types';

// const useStyles = makeStyles({
//     root: {
//         maxWidth: 345,
//     },
// });

type Props={
    productItem:Product
}
export default function ProductItemCard(props:Props) {
    // const classes = useStyles();
    const dispatch = useDispatch();
    const currentProduct=props.productItem;
    
    const addToCart=()=>{
     var addToCartDto:CartItemDto={
         productId:1,
         price:0,
         quantity:1
     };
   
     var result=(cartAction(addToCartDto));
   
     dispatch(result);
    };

    return (
        // <Card className={classes.root}>
    <Card >
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="140"
                    image={Anchor}
                    title="Contemplative Reptile"
                />
                <CardContent>
                <div>product</div>

    
                    <Typography gutterBottom variant="h5" component="h2">
                        Lizard
                   </Typography>
                   
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary">
                    Share
        </Button>
                

        <Button id={"addtocart"+currentProduct.id} className="addToCart" onClick={addToCart}> 
                    Add To Cart
        </Button>
            </CardActions>
        </Card>
    );
}