import React, { useEffect, useState } from "react";
import { productsApi } from "../api/productsApi";
import ProductItem from "./ProductItem";

const ProductsPage = () => {

    const [products, setProducts] = useState<any[]>([])

    useEffect(() => {
        productsApi.getProducts().then(res => {
            setProducts(res)
        })
    }, []);


    
    return (
        <>
          <ProductItem products={products} />
        </>
    )
}

export default ProductsPage;

