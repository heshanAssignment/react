import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ProductItemCard from './ProductItemCard';
import { Product } from './Types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }),
);

type Props = {
  products: Product[],
}

const ProductItem = (props: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>

        {props.products.map(p => <Grid key={p.id} item xs={3}>
          <ProductItemCard key={p.id} productItem={p}></ProductItemCard>
        </Grid>)}


      </Grid>
    </div>
  );
}

export default ProductItem;
